### 在局域网内搭建多节点私有以太坊网络

局域网内组建多节点以太坊网络实验，实验环境需要至少2台主机，如果没有，也可以尝试在本机开启2个geth进程，需要注意的是同一主机上做实验时，需要关闭ipc服务，否则可能存在问题。

## 1. 环境准备

两台linux主机，本人使用两个虚拟机，IP分别为：

```sh
192.168.137.181 #主机1
192.168.137.131 #主机2
```

主机上已经安装了geth客户端软件，如果未安装，可以参考本人的安装教程：

[Geth安装与启动](./Geth安装与启动.md)



## 2. 实验步骤如下：

### 2.1. 在主机一上初始化并启动geth 

创建一个rungeth目录

```sh
mkdir rungeth
```



编写genesis.json文件

```sh
{
    "config": {
      "chainId": 1008,
      "homesteadBlock": 0,
      "eip150Block": 0,
      "eip155Block": 0,
      "eip158Block": 0,
      "byzantiumBlock": 0,
      "constantinopleBlock": 0,
      "petersburgBlock": 0,
      "ethash": {}
    },
    "difficulty": "1",
    "gasLimit": "8000000",
    "alloc": {
      "7df9a875a174b3bc565e6424a0050ebc1b2d1d82": { "balance": "300000" },
      "f41c74c9ae680c1aa78f42e5647a62f353b7bdde": { "balance": "400000" }
    }
  }
```

初始化

```
geth init genesis.json --datadir ./data
```

创建账户(需要输入一个口令，测试时推荐简单一些，如果是实际环境，需要考虑安全问题)

```sh
geth account new --datadir data
```



启动


```
geth --datadir ./data --networkid 1008  --http --http.addr 0.0.0.0 --http.vhosts "*" --http.api "db,net,eth,web3,personal" --http.corsdomain "*" --snapshot=false --mine --miner.threads 1 --allow-insecure-unlock  console 2> 1.log
```

查看节点信息


```
> admin.nodeInfo
{
  enode: "enode://b011f4a90e93dbf9c93c7de90ae22330e47c7ddc4a6e4a23d5cfd7862394376244d8a47654da2f4a3593318e3586cdc397c469a091bf031a92876b6767c32b50@127.0.0.1:30303",
  enr: "enr:-KO4QN_2HyH4x5ssGVX3fFBY97VX40ZonPgxfxJZYIIimnLgcy9kAsB3KP7LQpcsYBIjwhZ8UA3WTA8ZB_QD4yRX0EqGAX9O0gIAg2V0aMfGhJ2pRlqAgmlkgnY0gmlwhH8AAAGJc2VjcDI1NmsxoQKwEfSpDpPb-ck8fekK4iMw5Hx93EpuSiPVz9eGI5Q3YoRzbmFwwIN0Y3CCdl-DdWRwgnZf",
  id: "5f3a9a6d3420aacb62de17b3e70f58fa95899fa8b2ac08ad5ba23cc19571913e",
  ip: "127.0.0.1",
  listenAddr: "[::]:30303",
  name: "Geth/v1.10.15-stable-8be800ff/linux-amd64/go1.17.5",
  ports: {
    discovery: 30303,
    listener: 30303
  },
  protocols: {
    eth: {
      config: {
        byzantiumBlock: 0,
        chainId: 1008,
        constantinopleBlock: 0,
        eip150Block: 0,
        eip150Hash: "0x0000000000000000000000000000000000000000000000000000000000000000",
        eip155Block: 0,
        eip158Block: 0,
        ethash: {},
        homesteadBlock: 0,
        petersburgBlock: 0
      },
      difficulty: 1,
      genesis: "0xc3638c5057e516005c90f14b439798979abfe13b491a15af85b0bdc514f97051",
      head: "0xc3638c5057e516005c90f14b439798979abfe13b491a15af85b0bdc514f97051",
      network: 1008
    },
    snap: {}
  }
}


```

查看peer节点数量


```
> net.peerCount
0

```


### 2. 在第二台主机上进行操作 

使用相同创世块文件初始化


```
geth init genesis.json --datadir data
```

创建账户(需要输入一个口令，测试时推荐简单一些，如果是实际环境，需要考虑安全问题)

```sh
geth account new --datadir data
```



启动


```
geth --datadir ./data --networkid 1008  --http --http.addr 0.0.0.0 --http.vhosts "*" --http.api "db,net,eth,web3,personal" --http.corsdomain "*" --snapshot=false --mine --miner.threads 1 --allow-insecure-unlock  console 2> 1.log
```

添加主机一的节点信息

***特别注意***：此encode信息为主机一查询到的geth信息，ip地址注意修改为局域网或映射后的公网地址。

```sh
admin.addPeer("enode://b011f4a90e93dbf9c93c7de90ae22330e47c7ddc4a6e4a23d5cfd7862394376244d8a47654da2f4a3593318e3586cdc397c469a091bf031a92876b6767c32b50@192.168.137.181:30303")
```



查看网络节点情况


```sh
> admin.peers
[{
    caps: ["eth/66", "snap/1"],
    enode: "enode://b011f4a90e93dbf9c93c7de90ae22330e47c7ddc4a6e4a23d5cfd7862394376244d8a47654da2f4a3593318e3586cdc397c469a091bf031a92876b6767c32b50@192.168.137.181:30303",
    id: "5f3a9a6d3420aacb62de17b3e70f58fa95899fa8b2ac08ad5ba23cc19571913e",
    name: "Geth/v1.10.15-stable-8be800ff/linux-amd64/go1.17.5",
    network: {
      inbound: false,
      localAddress: "192.168.137.131:52982",
      remoteAddress: "192.168.137.181:30303",
      static: true,
      trusted: false
    },
    protocols: {
      eth: {
        difficulty: 32124435,
        head: "0xee109c4dd975909f94dee32ff10df76884049c29b69442e20c03b688408ac489",
        version: 66
      },
      snap: {
        version: 1
      }
    }
}]

```

从当前结果来看，已经可以看到网络加入成功，在network内容中，可以看到两个节点ip



### 3. 测试数据同步情况

主机一查询账户信息


```sh
> eth.accounts
["0x5931b9cbdf5095fe31768a81283806f84f32a53e"]

```

主机一查询余额信息

```sh
> acc0=eth.accounts[0]
"0x5931b9cbdf5095fe31768a81283806f84f32a53e"
> eth.getBalance(acc0)
419500000000000000000

```

在***主机二***查询主机一账户余额

```sh
> eth.accounts
["0xe0ad2250ff8d3b92d334357c7c5773927010ada9"]
> acc1="0x5931b9cbdf5095fe31768a81283806f84f32a53e"
"0x5931b9cbdf5095fe31768a81283806f84f32a53e"
> eth.getBalance(acc1)
457500000000000000000

```
经过测试，主机二geth客户端可以获取主机一上账户的余额。感兴趣的同学可以再试试智能合约部署是否正常。


